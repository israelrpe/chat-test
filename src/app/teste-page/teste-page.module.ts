import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TestePageComponent } from './components/teste-page/teste-page.component';
import { TestePageRoutingModule } from './teste-page-routing.module';



@NgModule({
  declarations: [TestePageComponent],
  imports: [
    CommonModule,
    TestePageRoutingModule
  ]
})
export class TestePageModule { }
