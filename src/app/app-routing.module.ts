import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('../app/home-page/home-page.module').then((m) => m.HomePageModule),
  },
  {
    path: 'teste-page',
    loadChildren: () => import('../app/teste-page/teste-page.module').then((m) => m.TestePageModule),
  },
  {
    path: '**',
    redirectTo: 'home',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
