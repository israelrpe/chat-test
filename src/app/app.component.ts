import { Component, AfterViewInit, OnInit, NgZone } from '@angular/core';
import { ChatSalesForceService } from './chat-salesforce/services/chat-sales-force.service';
import LogRocket from 'logrocket';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit, OnInit {

  tooltipDisabled = false;

  constructor(private chatSalesForceService:ChatSalesForceService, private ngZone :NgZone){  }

  ngOnInit(): void {

    //Comment LogRocket init - To stop the chat error
    LogRocket.init("buws9w/portal-do-cliente");

    this.chatSalesForceService.snapInConnect();
  }

  ngAfterViewInit(): void {  }
}

declare global {
  interface Window {
    embedded_svc: any;
  }
}
