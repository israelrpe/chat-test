import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
	providedIn: 'root'
})
export class ChatSalesForceService {

	constructor() {
	}

	snapInConnect() {
		const gslbBaseURL = 'https://service.force.com';

		window.embedded_svc.domInitInProgress = false;
		window.embedded_svc.settings.displayHelpButton = true; //Or false
		window.embedded_svc.settings.language = 'pt-BR'; //For example, enter 'en' or 'en-US'
		window.embedded_svc.settings.targetElement = document.getElementById('snapinplaceholder');
		window.embedded_svc.settings.defaultMinimizedText = 'Chat - Attendance'; //(Defaults to Chat with an Expert)
		window.embedded_svc.settings.disabledMinimizedText = 'Offline';
		window.embedded_svc.settings.onlineLoadingText = 'Loading';
		window.embedded_svc.settings.enabledFeatures = ['LiveAgent'];
    window.embedded_svc.settings.entryFeature = 'LiveAgent';

    window.embedded_svc.settings.prepopulatedPrechatFields = {
      FirstName: "Teste",
      LastName: "Teste",
      Email: "teste@teste.com"
    };

		window.embedded_svc.init(
			environment.salesforceSettings.OrgUrlBase, //URL base da ORG Salesforce
			environment.salesforceSettings.UrlDeploymentLiveAgent, //URL do deploymento do Live Agent
			gslbBaseURL,
			environment.salesforceSettings.OrgId, //ID da ORG Salesforce
			environment.salesforceSettings.NomeDoDeploy, //Nome do Deploy
			{
				baseLiveAgentContentURL: environment.salesforceSettings.baseLiveAgentContentURL, // URL base do Live Agent Content
				deploymentId: environment.salesforceSettings.deploymentId, // Id do deployment
				buttonId: environment.salesforceSettings.buttonId, // Id do botão
				baseLiveAgentURL: environment.salesforceSettings.baseLiveAgentURL, // URL base do Live Agent
				eswLiveAgentDevName: environment.salesforceSettings.eswLiveAgentDevName, // Parametro de ID do deployment
				isOfflineSupportEnabled: false // Flag que indica se suporte offline está disponível
			}
		);
	}
}
