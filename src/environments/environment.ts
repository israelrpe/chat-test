// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  salesforceSettings: {
		OrgUrlBase: 'https://tcpterminal--Chatbot.cs17.my.salesforce.com',
		UrlDeploymentLiveAgent: 'https://chatbot-tcpliveagent.cs17.force.com/chatv2',
		baseLiveAgentContentURL: 'https://c.la1-c1cs-ord.salesforceliveagent.com/content',
		deploymentId: '57246000000cGXv',
		buttonId: '573460000008Yi0',
		baseLiveAgentURL: 'https://d.la1-c1cs-ord.salesforceliveagent.com/chat',
		eswLiveAgentDevName: 'EmbeddedServiceLiveAgent_Parent04I46000000XbcpEAC_1675bd1b82b',
		isOfflineSupportEnabled: false,
		OrgId: '00Dg0000006VTPS',
		NomeDoDeploy: 'TCPChatV2',
	},
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
