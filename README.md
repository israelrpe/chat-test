# ChatTest

LogRocket Salesforce bug test

## Install

`npm install`

`npm run serve`

`http://localhost:4300/`

The Chat tray icon will appear in the right footer of the page.

## Development server

Run `npm run serve` for a dev server. Navigate to `http://localhost:4300/`. The app will automatically reload if you change any of the source files.

